package ru.tsc.felofyanov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.felofyanov.tm.dto.request.ServerAboutRequest;
import ru.tsc.felofyanov.tm.dto.request.ServerVersionRequest;
import ru.tsc.felofyanov.tm.dto.response.ServerAboutResponse;
import ru.tsc.felofyanov.tm.dto.response.ServerVersionResponse;

public class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @SneakyThrows
    public static void main(String[] args) {
        SystemEndpointClient client = new SystemEndpointClient();
        client.connect();

        final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());

        final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());

        client.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }
}
