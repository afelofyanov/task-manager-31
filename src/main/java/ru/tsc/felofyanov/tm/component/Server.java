package ru.tsc.felofyanov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.endpoint.Operation;
import ru.tsc.felofyanov.tm.dto.request.AbstractRequest;
import ru.tsc.felofyanov.tm.dto.response.AbstractResponse;
import ru.tsc.felofyanov.tm.task.AbstractServerTask;
import ru.tsc.felofyanov.tm.task.ServerAcceptTask;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class Server {

    @NotNull
    private final Bootstrap bootstrap;
    @Getter
    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();
    @NotNull
    private final Dispatcher dispatcher = new Dispatcher();
    @Getter
    @Nullable
    public ServerSocket socketServer;

    public Server(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void submit(@NotNull AbstractServerTask task) {
        executorService.submit(task);
    }

    @SneakyThrows
    public void start() {
        @NotNull final Integer port = bootstrap.getPropertyService().getServerPort();
        socketServer = new ServerSocket(port);
        submit(new ServerAcceptTask(this));
    }

    @SneakyThrows
    public void stop() {
        if (socketServer == null) return;
        socketServer.close();
        executorService.shutdown();
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(Class<RQ> rqClass, Operation<RQ, RS> operation) {
        dispatcher.registry(rqClass, operation);
    }

    public Object call(final AbstractRequest request) {
        return dispatcher.call(request);
    }
}
