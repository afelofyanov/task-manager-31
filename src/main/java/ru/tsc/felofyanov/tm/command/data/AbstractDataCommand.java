package ru.tsc.felofyanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.command.AbstractCommand;
import ru.tsc.felofyanov.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String CATALOG = "./tmp";

    @NotNull
    public static final String FILE_BINARY = "tmp/data.bin";

    @NotNull
    public static final String FILE_BASE64 = "tmp/data.base64";

    @NotNull
    public static final String FILE_BACKUP = "tmp/backup.base64";

    @NotNull
    public static final String FILE_JAXB_XML = "tmp/data.jaxb.xml";

    @NotNull
    public static final String FILE_JAXB_JSON = "tmp/data.jaxb.json";

    @NotNull
    public static final String FILE_FASTERXML_XML = "tmp/data.fasterxml.xml";

    @NotNull
    public static final String FILE_FASTERXML_JSON = "tmp/data.fasterxml.json";

    @NotNull
    public static final String FILE_FASTERXML_YAML = "tmp/data.fasterxml.yaml";

    @NotNull
    public static final String JAVAX_XML_BIND_CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public static final String ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String APPLICATION_JSON = "application/json";

    public AbstractDataCommand() {
    }

    @NotNull
    public Domain getDomain() {
        @NotNull Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().set(domain.getProjects());
        serviceLocator.getTaskService().set(domain.getTasks());
        serviceLocator.getUserService().set(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }
}
